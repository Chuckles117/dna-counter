﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DNAString
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            int[] DNALetters = new int[4];

            for (int i = 0; i < DNAInput.Text.Length; i++)
            {
                string currentDNA = DNAInput.Text.Substring(i, 1);

                switch (currentDNA)
                {
                    case "A":
                        DNALetters[0]++;
                        break;
                    case "C":
                        DNALetters[1]++;
                        break;
                    case "G":
                        DNALetters[2]++;
                        break;
                    case "T":
                        DNALetters[3]++;
                        break;
                }

            }
            Console.WriteLine(DNALetters[0] + "\t"+ DNALetters[1] + "\t"+ DNALetters[2] + "\t"+ DNALetters[3]);

            string message = "A:"+DNALetters[0] + "\t" + "C:"+DNALetters[1] + "\t" + "G"+DNALetters[2] + "\t" + "T:"+DNALetters[3];
            string caption = "DNA Counter";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.Close();
            }
        }
    }
}
