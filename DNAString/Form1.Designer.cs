﻿namespace DNAString
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.DNAInput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "enter DNA string here:";
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(167, 155);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubmitButton.Size = new System.Drawing.Size(54, 23);
            this.SubmitButton.TabIndex = 1;
            this.SubmitButton.Text = "submit";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // DNAInput
            // 
            this.DNAInput.Location = new System.Drawing.Point(15, 96);
            this.DNAInput.Name = "DNAInput";
            this.DNAInput.Size = new System.Drawing.Size(356, 20);
            this.DNAInput.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 264);
            this.Controls.Add(this.DNAInput);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.TextBox DNAInput;
    }
}

